#include "file.h"

struct image open(char *filename) {
    FILE *in = fopen(filename, "rb");
    struct image img = {0};
    enum read_status cur_read_status = from_bmp(in, &img);
    if (cur_read_status == READ_OK) {
        fclose(in);
        return img;
    }
    fclose(in);
    struct image image_null = {0};
    return image_null;
}


int write(char *filename, struct image img) {
    FILE *out = fopen(filename, "wb");
    enum write_status cur_write_status = to_bmp(out, &img);
    if (cur_write_status != WRITE_OK) {
        image_destroy(&img);
        fclose(out);
        return 1;
    }
    fclose(out);
    return 0;
}
