#include "file.h"


int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "2 arguments are required");
        return 1;
    }
    struct image rotated_img;
    struct image img = open(argv[1]);
    if (img.height != 0) {
        rotated_img = rotate(img);
        int res = write(argv[2], rotated_img);
        image_destroy(&img);
        image_destroy(&rotated_img);
        if (res != 1) {
            return 0;
        }
    }
    image_destroy(&img);
    return 1;
}

