#include "bmp.h"

static uint8_t padding(uint8_t width) {
    uint8_t padding_size = 4 - (width * sizeof(struct pixel) % 4);
    return padding_size % 4;
}

struct bmp_header create_header(const struct image *img) {
    uint8_t padding_width = padding(img -> width);
    uint64_t image_size = (*img).height * ((*img).width * sizeof(struct pixel) + padding_width);
    struct bmp_header header = {
            .bfType = TYPE,
            .bfileSize = (uint32_t) image_size + sizeof(struct bmp_header),
            .bfReserved = RESERVED,
            .bOffBits = OFF_BITS,
            .biSize = SIZE,
            .biWidth = (uint32_t) (*img).width,
            .biHeight = (uint32_t) (*img).height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = (uint32_t) image_size,
            .biXPelsPerMeter = PPM,
            .biYPelsPerMeter = PPM,
            .biClrUsed = COLORS_USED,
            .biClrImportant = COLORS_IMPORTANT
    };
    return header;
}


enum read_status from_bmp(FILE *const in, struct image *const img) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if(header.bfType != TYPE){
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != BIT_COUNT){
        return READ_INVALID_BITS;
    }
    *img = image_create(header.biWidth, header.biHeight);
    uint8_t pad_width = padding((*img).width);
    for (size_t i = 0; i < (*img).height; i++) {
        fread((*img).data + (*img).width * i, sizeof(struct pixel), (*img).width, in);
        if (fseek(in, pad_width, SEEK_CUR) != 0) {
            return FILE_SEEK_ERROR;
        }
    }
    return READ_OK;

}

enum write_status to_bmp(FILE *const out, struct image const *img) {
    uint8_t padding_width = padding(img -> width);
    struct bmp_header header = create_header(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_HEADER_ERROR;
    }

    for (uint64_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, 3, img->width, out);
        fseek(out, padding_width, SEEK_CUR);
    }
    return WRITE_OK;
}
