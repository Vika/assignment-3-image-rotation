#include "image.h"

struct image image_create(uint64_t width, uint64_t height) {
    struct pixel *data = malloc(width * height * sizeof(struct pixel));
    if (data == NULL) {
        struct image image_null = {0};
        return image_null;
    }
    struct image image = {
            .height = height,
            .width = width,
            .data = data
    };
    return image;
}

void image_destroy(struct image *image) {
    image->height = 0;
    image->width = 0;
    free(image->data);
}


struct image rotate(struct image const source) {
    struct image result = image_create(source.height, source.width);
    for (uint64_t height_e = 0; height_e < result.height; height_e++) {
        for (uint64_t width_e = 0; width_e < result.width; width_e++) {
            result.data[height_e * result.width + width_e] = source.data[result.height * (result.width - width_e - 1) +
                                                                         height_e];
        }
    }
    return result;
}



