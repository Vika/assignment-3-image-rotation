#include "image.h"
#pragma once
#pragma pack(push, 1)

#define TYPE 0x4d42
#define RESERVED 0
#define PLANES 1
#define RESERVED 0
#define OFF_BITS 54
#define SIZE 40
#define COMPRESSION 0
#define PPM 2834
#define BIT_COUNT 24
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

struct bmp_header{
    uint16_t bfType;              // 0x4d42 | 0x4349 | 0x5450
    uint32_t bfileSize;           // размер файла
    uint32_t bfReserved;          // 0
    uint32_t bOffBits;            // смещение до поля данных, обычно 54 = x + biSize
    uint32_t biSize;              // размер струкуры в байтах: 40(BITMAPINFOHEADER) или 108(BITMAPV4HEADER) или 124(BITMAPV5HEADER)
    uint32_t biWidth;             // ширина в точках
    uint32_t biHeight;            // высота в точках
    uint16_t biPlanes;            // всегда должно быть 1
    uint16_t biBitCount;          // 0 | 1 | 4 | 8 | 16 | 24 | 32
    uint32_t biCompression;       // BI_RGB | BI_RLE8 | BI_RLE4 | BI_BITFIELDS | BI_JPEG | BI_PNG. Реально используется лишь BI_RGB
    uint32_t biSizeImage;         // Количество байт в поле данных. Обычно устанавливается в 0
    uint32_t biXPelsPerMeter;     // горизонтальное разрешение, точек на дюйм
    uint32_t biYPelsPerMeter;     // вертикальное разрешение, точек на дюйм
    uint32_t biClrUsed;           // Количество используемых цветов
    uint32_t biClrImportant;      // Количество существенных цветов. Можно считать, просто 0
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    FILE_SEEK_ERROR,
    READ_INVALID_SIGNATURE
};

enum  write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);
